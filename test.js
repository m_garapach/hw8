//1) відображення html коду гілками 
//2) innerHTML дає змогу вивести або змінити зміст html елементу. innerText дає зсогу вивести або змінити текст
//3) Дивлячись по ситуації, в якій гілці лежить елемент, чи є в нього id  або  class,
//   а кращого загального для всіх елементів методу певно немає, все залежно від ситуації



let searchP = document.body.getElementsByTagName('p')
for (let n of searchP) {
    n.style.backgroundColor = 'red';
}


let option = document.getElementById('optionsList');
console.log(option);

let perentOption = document.getElementById('optionsList')
console.log(perentOption.closest('.container'))

let childOption = document.getElementById('optionsList').childNodes
for (let i = 0; i < childOption.length; i++) {
    console.log(childOption[i])
}

let test = document.getElementById('testParagraph')
test.innerText = 'This is a paragraph'
console.log(test)

let elementsMainHeader = document.querySelector('.main-header').children
for (let i of elementsMainHeader) {
    i.classList.add('nav-item')
    console.log(i)
}

let section = document.querySelectorAll('.section-title')
for (let i of section) {
    i.classList.remove('section-title')
}
